_This document is a **proposal**. If you have opinions/comments create an issue
in the [gitlab repo](https://git.app.uib.no/uib/sky/issues) or you might even send us
a pull request._

# Kubernetes Resource Naming for UiB

This document provide guidelines for how we should name resources, namespaces
and manifest files for use with Kubernets at UiB.

The resources in the cluster are declared and configured by manifest files. We
use YAML format for these. Only put the specification for a single resource in
each file. Manifest files are maintained in git repos.

The manifest files have names on the form:

    <kind>-<name>.yaml

where &lt;kind> is the shortname of the resource type if available (see
`kubectl api-resources`). If there is no shortname exists use the full name in
singular form.

where &lt;name> is the value in the `.metadata.name` field.

The rules for naming different kind of resources are:

### Deployments (deploy)

Use the form:

    <system>

where &lt;system> is a short identifier for the system (or microservice)
that is realized by this deployment.

### Services (svc)

Use the form:

    <interface>
    <interface>-<variant>
    <system>
    <system>-<interface>

Service names should be in a form that that matches what label clients normally
use to think about the service.  For some this might be the same as the system,
but try to find a service name that can be stable even if the underlying system
that implements this service is replaced.

Interface names or variants might include the protocol name in some form.

### Secrets (secret)

Use the form:

    <service>-<...>
    <system>-<...>

Secrets are not defined in manifest files. That would defeat their purpose of
holding the information we don't want to be tracked in git.

### Jobs (job)

Use the form:

    <system>-<variant>

### Cronjobs (cj)

Same rules as for jobs.

### Namespaces (ns)

Use strings that match the environment from [azure-naming](azure-naming.md).

### Pods (po)

Pods are seldom named directly. They are normally created from jobs or
deployments.

## See also

[Configuration Best Practices](https://kubernetes.io/docs/concepts/configuration/overview/)
