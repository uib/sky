_This document is a **proposal**. If you have opinions/comments create an issue
in the [gitlab repo](https://git.app.uib.no/uib/sky/issues) or you might even send us
a pull request._

# Azure Resource Naming for UiB

This document provide guidelines for how we should name resources and resource
groups when we use Azure at UiB.

Resources and resouce groups are assigned unique names when they are created.
A name can not be changed once a resource has been created.  We want names to
have consistent form that makes it easy to group and filter resources.

The general pattern to follow is:

    resource: <opsorg>-<env>-<system>-<role><seq>?
    resgroup: <opsorg>-<env>
    resgroup: <opsorg>-<env>-<system>

where `<opsorg>` is the entity that is responsible for the operational
management of the resource.  The string "ita" is used for resources that the IT
department takes care of.  For experiments and personal development instances
make this string be the letter "z" followed by your UiB username.

where `<env>` denotes what system enviroment or system instance this resource
supports. The identifiers used are prod=production; test=testing;
dev=development; st=staging; tr=training instance.

where `<system>` is a short identifier for the system this resource supports.
Examples are "w3", "vle", "glass", "fs", "fspres". Use the string "none" for
resources that don't belong to any system (for resource groups you should just
leave it out).

where `<role>` is a short encoding of what role within the system his resource has. The
resource roles most common for us get short identifiers.
ci=container instance; vm=virtual machine; db=database (postgres, mysql,...);
st=storage account. Often the most obvious role identifier is the same as the resource type.

where `<seq>` is an optional sequence number. Use it when creating multiple
resources where all the other parts of the name is identical or when needed to
create uniqe names. Always make sequence numbers at least 2 digits long; thus
start counting with "01", "02", "03",...

Only use lower case lettes and never use the character "\_" as part of a name.


## Examples

These are some example names.

    rg: ita-prod-w3
    ita-prod-w3-ci01
    ita-prod-w3-ci02
    ita-prod-w3-st
    ita-prod-w3-db

    rg: ita-test-w3
    ita-test-w3-ci
    ita-test-w3-db

    rg: ita-prod
    ita-prod-none-cr (FQDN: ita-prod-none-cr.azurecr.io)

    rg: ita-prod-glass
    ita-prod-glass-st (FQDN: ita-prod-glass-st.blob.core.windows.net)
    ita-prod-glass-cdn (FQDN: ita-prod-glass-cdn.azureedge.net)

    rg: zgaa041-dev
    zgaa041-dev-w3-ci
    zgaa041-dev-hack-vm


## Tags

Up to 15 tags can be assigned to resources.  Tags are key/value pairs of
limited length (< 256 chars).  Aspects that don't fit into the name can be
represented by tags.  Tags are also reported in the usage records and can
thus be used to get insight into how costs came about.

Use of tags is optional at UiB. If you represent the following concepts
use tags of this form:

* `creator=<username>` specifies which user created this resource
* `ops=<username>` specifies which user can fix problems
* `env=<environment>` repeats the `<env>` part of the name (useful for filtering)
* `system=<system>` repeats the `<system>` part of the name (useful for filtering)
* `owner=<org-id>` which organisation unit is the system owner
* `cost=<code>` should the costs be filed somewhere nonstandard


## Rationale

Azure itself only use the resource names as identifiers for resources.  In that
regard the only concern is that names are unique within the relevant scope.
Azure also place a few restrictions on what characters can be used in names.
These restrictions vary slighly depending on type of resource.

Besides identifying resources — names communicate information to other people
and future versions of yourself.  There are many conflicting goals when we try
find a naming scheme:

* Names should look pretty and be easy on the eyes (not too much syntax and be short).
* Names should be informative
* Names should be easy to type (short and memorable — more important if we assume users reference names on the command line than selecting names from a list in a GUI).
* Names should make it easy to find resources in long lists (describe aspects of the resource and line up vertically).
* Names should make it easy to filter (describe aspects that match as substring with few false positives).
* Names should facilitate breaking up invoices into useful categories for what Azure usage costs us.
* Names should not look out-of-place even when relocated to some other scope (moved between resource groups or subscriptions — or even to a different cloud provider)

There is no way to change a name after a resource is created. A new resource
need to be created to take the place of the badly named one.  This can be hard
to do for production resources that require 100% uptime. We need some good
guidelines so that we get names enough right on the first try.

We use "ita" as the opsorg prefix for resources insted of "it" because it's more likely to be unique.

We use "z" as prefix for users because we want it to sort last and not end up
in conflict with org-names that we might want to introduce later (like "uib").

Some resource names ends up as part of domain names and must be globally unique:

* Domain name patterns by resource types:
    * Storage account: `https://<name>.blob.core.windows.net/` or more generally `https://<name>.<type>.core.windows.net/`
    * Container registry: `<name>.azurecr.io`
    * CDN endpoint: `<name>.azureedge.net`
    * PostgreSQL: `<name>.postgres.database.azure.com`
* Might want to include "uib" in such domain names
* These names should be easy to use (can make custom aliases under \*.uib.no for many of these).

Aspects that we might want represent in the name or be expressed in tags:

* cloud provider eg "azure", "aws", "uh-iaas",...
* system (identifier) eg "w3"
* service (identifier) eq "www"
* type eg "storage", "cdn", "container", "vm", "postgres", "db", ...
* environment eg "prod", "dev", "test",...
* location eg "northeurope", "ne",...
* creator (username)
* owner (department) eg "it" (who pays?)
* drift/operations (department) eq "it"
* role (within system) eg "lb", "cache", "db"
* seq# (instance number where many; or to make name unique) eg "01", "02",...
* subscription/resource group
* the legal organisation that owns the resource, ie "uib"

We avoid the character "\_" in names because Azure use this character to
separate parts of names it constructs for derived resources. One expection on this
is that they just append the string "PublicIP" onto the name of a VM for the IP address
resource that's automatically created.

Azure also injects a lot of resources into our namespace that we can't control
the naming of. Those we have experienced are:

* `cloud-shell-storage-<location>`
* `DefaultResourceGroup-<location>`
* `MC_<resgroup>_<resname>_<location>`
* `aks-<...>`
* `cs<randomhex>`
* `DefaultWorkspace-<randomhex>`
* `kubernetes<...>`
* `nodepool<...>`
* `ContainerInsights(<...>)`
* `SecurityCenterFree(<...>)`

This makes it really hard to end up with a clean and nice listing of resources.

### Comparison with NTNU

We gained access to NTNU's proposed naming standard document (version 2018-06-19).
These are some notes based on their text.

NTNU says that Microsoft has a _standard_ set of identifiers that match the
resource types; -vnet, -nic, -pip, -rg, -as, -vm, -func.  These names come from the
_suggested pattern_ column in the _Naming conventions_ document (see references below).

NTNU care about how subscriptions are named and repeat the subscription identifier
in the resource group names. Is that a good idea?

NTNU has different naming rules for each resource type. Is that a good idea?
Only thing in common is that all names ends with `-<type><seq>`.

NTNU have `<deployment-type>`, `<azure-region>` and `<fw-zone>` as part of the resource
group name.


### CMDB

There ought to be a common database of all resources across all cloud providers
as well as on-premise hosting.  This would probably be better served with a
naming standards that apply across providers.

Our [current CMDB](https://bs.uib.no/?module=cmdb&action=view) is
currently only focused on the mapping between machines and services.
How to reconceile this is for futher study.


### Other names

There are other stuff that might require names. Do we need standards for them? These include:

* Container image tags (on Docker Hub)
* BLOB containers (think s3 buckets — not to confuse them with the docker kind)
* Databases within a Postgres server
* Queues
* Firewall rules (for postgres)
* Policy definitions
* Policy assigments
* Vault keys
* Container instance `dnsNameLabel`
* Kubernetes resources

### Subscription naming

We do not discuss subscription naming here.  If we end up with users having
access to lots of subscriptions we will need a better system but the
subscription names can be changed at any time without affecting much (as the
subscription ids are UUIDs), so we are not too concerned about getting that
right before we start using Azure.

### Current machine naming standard

For comparison ITA currently names server machines — mostly vm's — using the following pattern:

    <env><location><system><role><seq>.uib.no

For example the name _p1ordlb01.uib.no_ can be decoded as:

* env=p (p=production, t=test, d=development)
* location=1 (1=nygårdsgt 5, 2=realfagsbygget)
* system=ord (ordbok.uib.no)
* role=lb (load balancer)
* seq=01 (instance #1)

### References

* <https://docs.microsoft.com/en-us/azure/architecture/best-practices/naming-conventions>
