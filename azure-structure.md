# Microsoft Azure at UiB

[Azure](https://azure.microsoft.com) is used by creating _resources_
and then by using the services the resources provide.  In order to create and
manage resources you need a suitable role connected to a _subscription_.  Once
you have that set up you should be able to login at
[portal.azure.com](https://portal.azure.com) with your _Fornavn.Etternavn@uib.no_
account and see and manage the resources belonging to the subscriptions you have access
to.

## Structure

The overall structures in Azure nests like this:

![Enrollment -> Departments -> Accounts -> Subscriptions -> Resouce Groups -> Resources](https://docs.microsoft.com/en-us/azure/security/media/governance-in-azure/security-governance-in-azure-fig1.png)

### Enterprise Enrollment

At the top level UiB have what's called an _Azure Enterprise Enrollment_ with
Microsoft. This is the agreement that UiB will pay the invoices that Microsoft
sends us based on the resources consumed. The agreement we signed is based on
negotiations that [GÉANT](https://www.geant.org) (via Uninett) has done for us.

### Departments

Within an enrollment we can divide the Azure world into what Microsoft calls
_departments_. For UiB we let the departments within Azure be:

* IT-avdelingen
* Universitetsbiblioteket
* Det medisinske fakultet
* Det matematisk-naturvitenskapelige fakultet
* ...
* Universitetsmuseet i Bergen

More will come as need arises.  The _Enterprise Administrator_ for our
enrollment can create more. This role is currently assigned to
Jan Kristian Walde Johnsen.

Resources supporting systems that provide shared services for all will be found
under the "IT-avdelingen" organisation.

There will be separate invoices from Microsoft for resources consumed within
each department. The main reason for creating new departments appears to be to
reduce the amount of internal invoicing that takes place between units within
UiB.  The structure is not aimed to reflect the true hierarchy of the UiB
organisation.

For each department there will be at least one _Account Owner_. These belong to
people that can create the subscriptions and assign the initial subscription
owners. The following subscriptions are currently active:

* Department: IT-avdelingen (account owner: Raymond Kristiansen)
    * Subscription: Applikasjoner
    * Subscription: Faggruppe Windows
    * Subscription: Virtualiseringsteam

### Subscriptions

This is the level that ordinary users of Azure relate to. Users get access to
subscriptions by being assigned roles within. Users with the role _owner_ or _user access administrator_ can
manage what roles other users have within the subscription, and can thus give
people access.  Users with the role _contributior_ can create and manage
resources.

Policies can be assigned to subscriptions in order to limit what kind of
resources can be created or to enforce naming and tagging standards.
Currently we don't use policies.

### Resource groups

A resource group is a container of resources within a subscription.
All resources must belong to a single resource group.  If a resource
group is deleted all resources within is deleted as well.

Policies can also be assigned at the resource group level.

### Azure AD

UiB has a so called _tenant_ with Microsoft.  For us it is represented with
the domain name _universityofbergen.onmicrosoft.com_.

Within the tenant Microsoft provide a directory service called Azure AD — which
contains a collection of so called _work or school accounts_. For UiB these are set up
to match the UiB accounts and are identified by strings like "Fornavn.Etternavn@uib.no".
These are the accounts we should use when accessing Azure for work. When you
log in using a school account you will be redirected to _adfs.uhad.no_ for
verification of your password.

Azure AD also contain groups. For UiB the _work or school accounts_ must be members of the
"Azure\_AccessPortal" group for access to the portal to be granted.  Access to Azure can have
real consequences for production services and UiB's budget, so we require
additional factors when authenticating.

The same tenant and Azure AD instance supports both Office 365 and our Azure
enrollment.
