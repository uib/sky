This repo contains documentation on how we use general cloud resources at the
University of Bergen (UiB).

We have full access to [NREC](https://www.nrec.no).  This access is "free"
for UiB users — UiB pays by running parts of the OpenStack infrastructure.

We currently have agreements and structure in place to use [Microsoft Azure](https://azure.microsoft.com).

We have signed an agreement to use [AWS](https://aws.amazon.com), but there is still work outstanding before our users get accounts and access.

We have projects using [IBM Cloud](https://www.ibm.com/cloud/), but no general agreement.
